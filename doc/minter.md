The minter is able to mint new TRB tokens from:
- Injection of veRETRO in the illiquid treasury
- Injection of Retro ALM LP tokens into the liquid treasury

In practice we expect:
- Weekly mint from ALM LP tokens obtained from swap fees and bribes, to be sent to the reliquary as rewards
- Permisioned mint from veRETRO, with vesting constraints
  - The initial mint from OPP's veRETRO for example

Later we can enable permissionless mint from veRETRO by formalizing the contraints as code & protocol rules.

## Minting from veRETRO

What we want to avoid is the possibility of instant dumping of minted TRB.

Assume the protocol currently owns $X worth of TRB-CASH as POL, composed of $X1 worth of CASH and $X2 worth of TRB => $X = $X1 + $X2.

Assume an entity wants to mint $Y worth of TRB with a veRETRO, and the expected APR of veRETRO is y% (accounting for expected dilution).

Then $Y worth of TRB should earn $(Y * y / 100) over a year => I guess we should use that value (discounted) to compute the vesting.

We should also use $Y / $X1, the higher $Y compared to $X1, the longer the vesting because our liquidity could not absorbe easily the created value.

We can offer some POL deal to the minter:
- Mint vestedTRB from veRETRO
- Inject vestedTRB / CASH into the liquid treasuty => it becomes TRB / CASH => it mints new liquid TRB for the minter

What we want to compute is an equilibrium relationship: if all vestedTRB are injected as vestedTRB / CASH, and all resulting liquid TRB are dumped, then the price don't move => the minter would have injected its own exit liquidity by doing such action.

All of this requires some R&D before being usable, so we will only have initial mint from veRETRO at release time, with vesting and POL injection handled manually or with a simple dedicated contract.

## ALM LP tokens references

- Gamma ALM ERC20: https://polygonscan.com/address/0x839d5b563e06b4abdf02d1a9d58d8ed33592c291
- Ichi ALM ERC20: https://polygonscan.com/token/0xb4eafb818c6c9f4862c95502075667b7b662c573
