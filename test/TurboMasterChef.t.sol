// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import "openzeppelin-contracts/contracts/mocks/token/ERC20Mock.sol";
import "../src/TurboMasterChef.sol";

contract TestTurboMasterChef is Test {
    TurboMasterChef public masterChef;
    ERC20Mock newToken;

    function setUp() public {
        masterChef = new TurboMasterChef();
        newToken = new ERC20Mock();

        masterChef.grantRole(keccak256("OPERATOR"), address(this));
    }

    function testAddTokenInWhiteList() public {
        masterChef.addTokenInWhiteList(address(newToken));
        masterChef.deposit(address(newToken), 0);
    }

    function testCorrectWrappedTokenIsCreated() public {
        masterChef.addTokenInWhiteList(address(newToken));
        ERC20 wrappedToken =
            ERC20(masterChef.tokenToWrappedToken(address(newToken)));
        assertEq(wrappedToken.name(), "Wrapped Turbo ERC20Mock");
        assertEq(wrappedToken.symbol(), "WTURBOE20M");
        assertEq(
            address(newToken),
            masterChef.wrappedTokenToToken(address(wrappedToken))
        );
    }

    function testFailDepositTokenNotWhitelisted() public {
        masterChef.deposit(address(newToken), 1);
    }

    function testFailOnUnauthorizedAddTokenInWhiteList() public {
        vm.prank(address(1));
        masterChef.addTokenInWhiteList(address(newToken));
    }

    function testDeposit(uint256 amount) public {
        masterChef.addTokenInWhiteList(address(newToken));
        newToken.mint(address(1), amount);

        vm.prank(address(1));
        IERC20(newToken).approve(address(masterChef), amount);

        vm.prank(address(1));
        masterChef.deposit(address(newToken), amount);

        assertEq(newToken.balanceOf(address(1)), 0);

        assertEq(
            ERC20(masterChef.tokenToWrappedToken(address(newToken))).balanceOf(
                address(1)
            ),
            amount
        );
    }

    function testFailWithdrawUnwrappedToken() public {
        masterChef.withdraw(address(newToken), 1);
    }

    function testWithdraw(uint256 amount) public {
        masterChef.addTokenInWhiteList(address(newToken));
        WrappedTurboERC20 wrappedNewToken =
            WrappedTurboERC20(masterChef.tokenToWrappedToken(address(newToken)));

        newToken.mint(address(masterChef), amount); //create a treasury in the masterChef

        vm.prank(address(masterChef));
        wrappedNewToken.mint(address(1), amount);
        assertEq(wrappedNewToken.balanceOf(address(1)), amount); //check if our token minter correctly

        vm.prank(address(1));
        masterChef.withdraw(address(wrappedNewToken), amount);

        assertEq(newToken.balanceOf(address(masterChef)), 0);
        assertEq(newToken.balanceOf(address(1)), amount);
        assertEq(wrappedNewToken.balanceOf(address(1)), 0);
    }

    function testWrongAmountWithdrawMustRevert() public {
        masterChef.addTokenInWhiteList(address(newToken));
        WrappedTurboERC20 wrappedNewToken =
            WrappedTurboERC20(masterChef.tokenToWrappedToken(address(newToken)));

        newToken.mint(address(masterChef), 10); //create more tokens than what we want to withdraw

        vm.prank(address(masterChef));
        wrappedNewToken.mint(address(1), 1);
        assertEq(wrappedNewToken.balanceOf(address(1)), 1);

        vm.prank(address(1));
        vm.expectRevert();
        masterChef.withdraw(address(wrappedNewToken), 2);
    }

    function testFailedMintWrappedTokenWhenNotOwner() public {
        masterChef.addTokenInWhiteList(address(newToken));
        WrappedTurboERC20 wrappedNewToken =
            WrappedTurboERC20(masterChef.tokenToWrappedToken(address(newToken)));

        wrappedNewToken.mint(address(1), 1);
    }

    function testRemoveTokenFromWhiteList() public {
        masterChef.addTokenInWhiteList(address(newToken));
        masterChef.deposit(address(newToken), 0);
        masterChef.removeTokenFromWhiteList(address(newToken));
        vm.expectRevert();
        masterChef.deposit(address(newToken), 0);
    }

    function testWithdrawTokenRemovedFromWhiteList(uint256 amount) public {
        masterChef.addTokenInWhiteList(address(newToken));
        masterChef.deposit(address(newToken), 0);
        WrappedTurboERC20 wrappedNewToken =
            WrappedTurboERC20(masterChef.tokenToWrappedToken(address(newToken)));

        newToken.mint(address(masterChef), amount); //create a treasury in the masterChef

        vm.prank(address(masterChef));
        wrappedNewToken.mint(address(1), amount);
        assertEq(wrappedNewToken.balanceOf(address(1)), amount); //check if our token minter correctly

        masterChef.removeTokenFromWhiteList(address(newToken)); //remove token to test if we can still withdraw

        vm.prank(address(1));
        masterChef.withdraw(address(wrappedNewToken), amount);

        assertEq(newToken.balanceOf(address(masterChef)), 0);
        assertEq(newToken.balanceOf(address(1)), amount);
        assertEq(wrappedNewToken.balanceOf(address(1)), 0);
    }
}
