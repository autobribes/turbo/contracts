// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8;

import "forge-std/Test.sol";
import "forge-std/console.sol";
import "src/TurboMinterFromERC20.sol";
import "@openzeppelin/contracts/mocks/token/ERC20Mock.sol";

contract TurboMinterFromERC20Test is Test {
    TurboMinterFromERC20 minter;
    ERC20Mock trbToken;
    TurboPricerMock pricer;
    address treasury;
    TRBTokenDistributorMock distributor;

    ERC20Mock treasuryToken;
    ERC20Mock notTreasuryToken;

    function setUp() public {
        trbToken = new ERC20Mock();
        treasuryToken = new ERC20Mock();
        notTreasuryToken = new ERC20Mock();

        treasury = address(0xc2ba);
        pricer = new TurboPricerMock(treasuryToken, treasury);
        distributor = new TRBTokenDistributorMock(trbToken);
        minter = new TurboMinterFromERC20(
            ITRBToken(address(trbToken)), pricer, treasury, distributor
        );
    }

    function testRevertWhenMintWhileTreasuryValueIsZero(uint256 inAmount)
        public
    {
        vm.expectRevert(TurboMinterFromERC20.ZeroTreasuryBackingValue.selector);
        minter.mintFromERC20(treasuryToken, inAmount, 1, type(uint256).max);
    }

    function testRevertWhenMintWhileTRBSupplyIsZero(
        uint256 treasuryAmount,
        uint256 inAmount
    )
        public
    {
        vm.assume(treasuryAmount > 0);
        treasuryToken.mint(treasury, treasuryAmount);
        vm.expectRevert(TurboMinterFromERC20.ZeroTRBSupply.selector);
        minter.mintFromERC20(treasuryToken, inAmount, 1, type(uint256).max);
    }

    function testQuoteMintFromZeroERC20ValueIsZero(
        uint256 treasuryAmount,
        uint256 trbSupply
    )
        public
    {
        vm.assume(treasuryAmount > 0 && trbSupply > 0);
        treasuryToken.mint(treasury, treasuryAmount);
        trbToken.mint(address(this), trbSupply);
        uint256 trbAmount = minter.quoteMintFromERC20(treasuryToken, 0);
        assertEq(trbAmount, 0);
    }

    function testRevertWhenMintFromNotTreasuryERC20(
        uint256 treasuryAmount,
        uint256 inAmount,
        uint256 trbSupply
    )
        public
    {
        vm.assume(treasuryAmount > 0 && trbSupply > 0);
        treasuryToken.mint(treasury, treasuryAmount);
        trbToken.mint(address(this), trbSupply);

        vm.expectRevert(TurboPricerMock.NotTreasuryToken.selector);
        minter.mintFromERC20(notTreasuryToken, inAmount, 1, type(uint256).max);
    }

    function testRevertWhenMintOutputAmountIsInsufficient(
        uint256 treasuryAmount,
        uint256 inAmount,
        uint256 trbSupply
    )
        public
    {
        vm.assume(
            treasuryAmount > 0 && inAmount > 0 && trbSupply > 0
                && inAmount < type(uint256).max - treasuryAmount
                && inAmount < type(uint256).max / trbSupply
        );
        treasuryToken.mint(treasury, treasuryAmount);
        treasuryToken.mint(address(this), inAmount);
        trbToken.mint(address(this), trbSupply);
        uint256 trbAmount = minter.quoteMintFromERC20(treasuryToken, inAmount);
        vm.assume(trbAmount > 0);

        // Inflate treasury, which should decrease mint amount
        treasuryToken.mint(treasury, treasuryAmount);
        vm.expectRevert(
            TurboMinterFromERC20.InsufficientMintOutputAmount.selector
        );
        minter.mintFromERC20(treasuryToken, inAmount, trbAmount, trbAmount);
    }

    function testRevertWhenMintOutputAmountIsInflated(
        uint256 treasuryAmount,
        uint256 inAmount,
        uint256 trbSupply
    )
        public
    {
        vm.assume(
            treasuryAmount > 1 && inAmount > 0 && trbSupply > 0
                && inAmount < type(uint256).max - treasuryAmount
                && inAmount < type(uint256).max / trbSupply
        );
        treasuryToken.mint(treasury, treasuryAmount);
        treasuryToken.mint(address(this), inAmount);
        trbToken.mint(address(this), trbSupply);
        uint256 trbAmount = minter.quoteMintFromERC20(treasuryToken, inAmount);
        vm.assume(trbAmount > 0);

        // Burn from treasury, which should increase mint amount
        treasuryToken.burn(treasury, treasuryAmount / 2);
        vm.expectRevert(TurboMinterFromERC20.InflatedMintOutputAmount.selector);
        minter.mintFromERC20(treasuryToken, inAmount, trbAmount, trbAmount);
    }

    function testRevertWhenMintRangeIsInvalid(
        uint256 inAmount,
        uint256 minOutAmount,
        uint256 maxOutAmount
    )
        public
    {
        // Force an invalid range:
        vm.assume(minOutAmount == 0 || maxOutAmount < minOutAmount);
        vm.expectRevert(TurboMinterFromERC20.InvalidMintRange.selector);
        minter.mintFromERC20(
            treasuryToken, inAmount, minOutAmount, maxOutAmount
        );
    }

    function testQuoteMintFromERC20ReturnsExpectedAmount(
        uint256 treasuryAmount,
        uint256 inAmount,
        uint256 trbSupply
    )
        public
    {
        vm.assume(
            treasuryAmount > 0 && inAmount > 0 && trbSupply > 0
                && inAmount < type(uint256).max - treasuryAmount
                && inAmount < type(uint256).max / trbSupply
        );
        treasuryToken.mint(treasury, treasuryAmount);
        treasuryToken.mint(address(this), inAmount);
        trbToken.mint(address(this), trbSupply);
        uint256 trbAmount = minter.quoteMintFromERC20(treasuryToken, inAmount);
        vm.assume(trbAmount > 0);
        assertEq(trbAmount, trbSupply * inAmount / treasuryAmount);
    }

    function testMintFromERC20DistributesExpectedAmount(
        uint256 treasuryAmount,
        uint256 inAmount,
        uint256 trbSupply,
        address minterAccount
    )
        public
    {
        vm.assume(
            treasuryAmount > 0 && inAmount > 0 && trbSupply > 0
                && inAmount < type(uint256).max - treasuryAmount
                && inAmount < type(uint256).max / trbSupply
        );
        vm.assume(minterAccount != address(0));
        treasuryToken.mint(treasury, treasuryAmount);
        treasuryToken.mint(minterAccount, inAmount);
        trbToken.mint(address(this), trbSupply);
        uint256 trbAmount = minter.quoteMintFromERC20(treasuryToken, inAmount);
        vm.assume(trbAmount > 0);

        vm.startPrank(minterAccount);
        treasuryToken.approve(address(minter), inAmount);
        uint256 mintedAmount =
            minter.mintFromERC20(treasuryToken, inAmount, trbAmount, trbAmount);
        vm.stopPrank();

        assertEq(trbAmount, mintedAmount);

        assertEq(trbToken.balanceOf(address(distributor)), trbAmount);
        assertEq(
            distributor.minterAccountToMintedAmount(minterAccount), trbAmount
        );
    }
}

/// This mock pricer just compute the value as the amount of treasuryToken owned.
/// A real pricer would compute the total value in some quote currency (USD, ETH or MATIC)
/// and price ERC20 tokens in that same currency.
contract TurboPricerMock is ITurboPricer {
    IERC20 treasuryToken;
    address treasury;

    constructor(IERC20 _treasuryToken, address _treasury) {
        treasuryToken = _treasuryToken;
        treasury = _treasury;
    }

    function getBackingValue() external view returns (uint256) {
        return treasuryToken.balanceOf(treasury);
    }

    /// Assume getERC20Value reverts if the token is not allowed in the treasury
    function getERC20Value(
        IERC20 token,
        uint256 amount
    )
        external
        view
        returns (uint256)
    {
        if (address(token) != address(treasuryToken)) {
            revert NotTreasuryToken();
        }
        return amount;
    }

    error NotTreasuryToken();
}

/// This mock distributor just keep minted TRB for itself and record minted amount per account.
/// A real distributor would send the TRB somewhere else (vesting contract, revenue distribution, treasury, ...)
contract TRBTokenDistributorMock is ITRBTokenDistributor {
    IERC20 trbToken;
    mapping(address => uint256) public minterAccountToMintedAmount;

    constructor(IERC20 _trbToken) {
        trbToken = _trbToken;
    }

    function depositTRB(uint256 amount, address minterAccount) external {
        trbToken.transferFrom(msg.sender, address(this), amount);
        minterAccountToMintedAmount[minterAccount] += amount;
    }
}
