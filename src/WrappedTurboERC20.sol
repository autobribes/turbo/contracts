// SPDX-License-Identifier: UNLICENSED

import "openzeppelin-contracts/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-contracts/contracts/access/Ownable.sol";

pragma solidity ^0.8.13;

contract WrappedTurboERC20 is ERC20, Ownable {
    bytes32 internal constant OWNER = keccak256("OWNER");

    constructor(
        string memory name_,
        string memory symbol_
    )
        ERC20(name_, symbol_)
        Ownable(msg.sender)
    {}

    function mint(address account, uint256 amount) public onlyOwner {
        _mint(account, amount);
    }

    function burn(address account, uint256 amount) public onlyOwner {
        _burn(account, amount);
    }
}
