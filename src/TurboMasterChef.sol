// SPDX-License-Identifier: UNLICENSED

import
    "openzeppelin-contracts/contracts/access/extensions/AccessControlEnumerable.sol";
import "openzeppelin-contracts/contracts/token/ERC20/utils/SafeERC20.sol";
import "./WrappedTurboERC20.sol";

pragma solidity ^0.8.13;

contract TurboMasterChef is AccessControlEnumerable {
    using SafeERC20 for IERC20;
    /// @dev Access control roles.

    bytes32 internal constant OPERATOR = keccak256("OPERATOR");

    mapping(address => bool) private tokenWhiteList;
    mapping(address => address) public tokenToWrappedToken;
    mapping(address => address) public wrappedTokenToToken;

    /// @notice The token isn't whitelisted by the protocole
    error TokenNotWhitelisted();
    /// @notice The token isn't wrapped in the protocole
    error WrappedTokenNotExisting();
    /// @notice The transfer from user to pool failed
    error TransferFailed();

    modifier onlyWhiteListed(address _address) {
        if (!tokenWhiteList[_address]) {
            revert TokenNotWhitelisted();
        }
        _;
    }

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function addTokenInWhiteList(address tokenToAdd)
        public
        onlyRole(OPERATOR)
    {
        tokenWhiteList[tokenToAdd] = true;
        if (tokenToWrappedToken[tokenToAdd] == address(0)) {
            WrappedTurboERC20 newWrappedToken = new WrappedTurboERC20(
                string.concat("Wrapped Turbo ", ERC20(tokenToAdd).name()),
                string.concat("WTURBO", ERC20(tokenToAdd).symbol())
            );
            tokenToWrappedToken[tokenToAdd] = address(newWrappedToken);
            wrappedTokenToToken[address(newWrappedToken)] = tokenToAdd;
        }
    }

    function deposit(
        address token,
        uint256 amount
    )
        public
        onlyWhiteListed(token)
    {
        IERC20(token).safeTransferFrom(msg.sender, address(this), amount);
        WrappedTurboERC20(tokenToWrappedToken[token]).mint(msg.sender, amount);
    }

    function withdraw(address wrappedToken, uint256 amount) public {
        if (wrappedTokenToToken[wrappedToken] == address(0)) {
            revert WrappedTokenNotExisting();
        }

        IERC20 token = IERC20(wrappedTokenToToken[wrappedToken]);
        token.safeTransfer(msg.sender, amount);
        WrappedTurboERC20(tokenToWrappedToken[address(token)]).burn(
            msg.sender, amount
        );
    }

    function removeTokenFromWhiteList(address _tokenToDisable)
        public
        onlyRole(OPERATOR)
        onlyWhiteListed(_tokenToDisable)
    {
        tokenWhiteList[_tokenToDisable] = false;
    }
}
