// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

/// A Minter contract accepting ERC20 deposits as backing to mint new TRB tokens in return.
contract TurboMinterFromERC20 {
    using SafeERC20 for IERC20;

    ITRBToken public immutable trbToken;
    ITurboPricer public immutable pricer;
    address public immutable treasury;
    ITRBTokenDistributor public immutable distributor;

    /// @param _trbToken Address of the TRB token
    /// @param _pricer Address of a pricer contract to get total value of treasury and value of ERC20 tokens to deposit
    /// @param _treasury Address of the treasury
    /// @param _distributor Address of a distributor contract for minted TRB tokens
    constructor(
        ITRBToken _trbToken,
        ITurboPricer _pricer,
        address _treasury,
        ITRBTokenDistributor _distributor
    ) {
        trbToken = _trbToken;
        pricer = _pricer;
        treasury = _treasury;
        distributor = _distributor;
    }

    /// Compute amount of TRB tokens that would be minted from a given amount of ERC20 tokens to deposit.
    /// Should be called before mintFromERC20 to compute an expected mint range.
    /// @param _token Address of token to deposit into the liquid treasury as backing for minted TRB
    /// @param _inAmount Amount of token to deposit
    function quoteMintFromERC20(
        IERC20 _token,
        uint256 _inAmount
    )
        public
        view
        returns (uint256)
    {
        uint256 backingValue = pricer.getBackingValue();
        if (backingValue == 0) {
            revert ZeroTreasuryBackingValue();
        }
        uint256 trbSupply = trbToken.totalSupply();
        if (trbSupply == 0) {
            revert ZeroTRBSupply();
        }
        uint256 tokenValue = pricer.getERC20Value(_token, _inAmount); // Should revert if _token is not allowed in treasury
        return (trbToken.totalSupply() * tokenValue) / backingValue;
    }

    /// Standard mint filling the liquid treasury and distributing to a known distributor.
    /// Using TurboMasterChef wrapped tokens that we can price relative to the treasury.
    /// Anyone can mint TRB using this function since tokens are sent to the distributor
    /// We ensure they are not immediately dumpable and will be distributed according to some policy.
    /// The pricer is responsible to revert if _token is not allowed in treasury.
    /// @param _token Address of token to deposit into the liquid treasury as backing for minted TRB
    /// @param _inAmount Amount of token to deposit
    /// @param _minOutAmount Minimum expected amount of TRB to mint, to avoid front-running attacks
    /// @param _maxOutAmount Maximal expected amount of TRB to mint, to avoid front-running attacks
    function mintFromERC20(
        IERC20 _token,
        uint256 _inAmount,
        uint256 _minOutAmount,
        uint256 _maxOutAmount
    )
        external
        returns (uint256)
    {
        if (_minOutAmount == 0 || _maxOutAmount < _minOutAmount) {
            revert InvalidMintRange();
        }

        // Compute amount of TRB to mint and ensure it is in expected range
        uint256 trbAmount = quoteMintFromERC20(_token, _inAmount);
        if (trbAmount < _minOutAmount) {
            revert InsufficientMintOutputAmount();
        }
        if (trbAmount > _maxOutAmount) {
            revert InflatedMintOutputAmount();
        }

        // Mint and deposit new TRB to distributor, which is responsible for the distribution policy
        trbToken.mint(address(this), trbAmount);
        trbToken.approve(address(distributor), trbAmount);
        distributor.depositTRB(trbAmount, msg.sender);

        // Transfer input ERC20 tokens to the treasury as backing:
        _token.safeTransferFrom(msg.sender, treasury, _inAmount);

        return trbAmount;
    }

    /// @notice Not enough tokens will be minted wrt the specified range
    error InsufficientMintOutputAmount();
    /// @notice Too many tokens will be minted wrt the specified range
    error InflatedMintOutputAmount();
    /// @notice The mint range is invalid: minAmount is zero or maxAmount is less than minAmount
    error InvalidMintRange();
    /// @notice The baking value of the treasury is zero so mint amount is undefined
    error ZeroTreasuryBackingValue();
    /// @notice The total supply of TRB so mint amount is always zero
    error ZeroTRBSupply();
}

interface ITRBToken is IERC20 {
    function mint(address account, uint256 amount) external;
}

/// The pricer is responsible to compute backing value of the treasury and value of
/// assets that can be injected inside to mint new TRB tokens.
interface ITurboPricer {
    function getBackingValue() external view returns (uint256);

    /// Assume getERC20Value reverts if the token is not allowed in the treasury
    function getERC20Value(
        IERC20 token,
        uint256 amount
    )
        external
        view
        returns (uint256);
}

/// The distributor is responsible of the TRB distribution policy accross the protocol.
/// Initially expected to distribute rewards to the staking pools, may evolve later.
interface ITRBTokenDistributor {
    function depositTRB(uint256 amount, address minterAccount) external;
}
